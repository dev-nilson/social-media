import { useState } from "react";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import { LineWobble } from "@uiball/loaders";
import { updateImage } from "@/utils/helpers";

function Cover({ url, editable, onChange }) {
  const [isUploading, setIsUploading] = useState(false);
  const supabase = useSupabaseClient();
  const session = useSession();

  const updateCover = async (e) => {
    const file = e.target.files?.[0];

    if (file && file.type.startsWith("image/")) {
      setIsUploading(true);
      await updateImage(supabase, session.user.id, file, "covers", "cover_url");
      setIsUploading(false);

      if (onChange) onChange();
    }
  };

  return (
    <div className="h-24 sm:h-40 overflow-hidden flex justify-center items-center relative border-b">
      <div>
        <img src={url} />
      </div>
      {isUploading && (
        <div className="absolute inset-0 bg-white bg-opacity-80 flex items-center justify-center">
          <LineWobble size={100} color="#231F20" />
        </div>
      )}
      {editable && (
        <div className="absolute right-0 top-0 mx-1 my-2">
          <label className="bg-white rounded-full cursor-pointer p-1 text-md border">
            <input
              className="hidden"
              type="file"
              onChange={updateCover}
              accept="image/*"
            />
            <span>✏️</span>
          </label>
        </div>
      )}
    </div>
  );
}

export default Cover;
