import Profile from "../Profile/Profile";
import RootLayout from "../RootLayout/RootLayout";

function ProfileLayout({ children }) {
  return (
    <RootLayout>
      <div className="max-w-3xl m-auto">
        <Profile />
      </div>
      <div>{children}</div>
    </RootLayout>
  );
}

export default ProfileLayout;
