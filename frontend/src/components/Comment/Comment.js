import { useState } from "react";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import { commentPost } from "@/utils/helpers";
import Avatar from "../Avatar/Avatar";
import ReactTimeAgo from "react-time-ago";

function Comment({ postId, avatar, text, date }) {
  const [comment, setComment] = useState("");
  const supabase = useSupabaseClient();
  const session = useSession();

  const handleComment = async (e) => {
    e.preventDefault();
    await commentPost(supabase, postId, session.user.id, comment);
    setComment("");
  };

  return (
    <div className="flex gap-3 py-2 items-center">
      <div>
        <Avatar url={avatar} small />
      </div>
      {text ? (
        <div className="flex items-center justify-between w-full">
          <p className="text-sm">{text}</p>
          <p className="text-gray-500 text-xs">
            <ReactTimeAgo date={Date.parse(date)} />
          </p>
        </div>
      ) : (
        <form
          className="border grow rounded-md text-sm relative h-9"
          onSubmit={handleComment}
        >
          <input
            className="block w-full p-2 px-3 rounded-md resize-none overflow-hidden h-full"
            placeholder="Escribir comentario..."
            value={comment}
            onChange={(e) => setComment(e.target.value)}
          />
        </form>
      )}
    </div>
  );
}

export default Comment;
