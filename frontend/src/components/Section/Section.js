import { useEffect, useState } from "react";
import { useSupabaseClient } from "@supabase/auth-helpers-react";
import Post from "../Post/Post";
import Contact from "../Contact/Contact";
import { getPostsByCategory } from "@/utils/helpers";
import Card from "../Card/Card";

function Section({ activeLink, username }) {
  const supabase = useSupabaseClient();
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    if (!username) return;
    getUserPosts();
  }, [username, activeLink]);

  const getUserPosts = () => {
    let category = "";
    if (activeLink === "compro") category = "buy";
    if (activeLink === "vendo") category = "sell";

    getPostsByCategory(supabase, username, category).then((res) => {
      setPosts(res.data);
    });
  };

  return (
    <div className="max-w-3xl m-auto">
      {activeLink === "compro" && (
        <div>
          {posts.length > 0 ? (
            posts.map((post) => <Post key={post.created_at} {...post} />)
          ) : (
            <Card>
              <p className="mb-2 text-sm">No hay publicaciones aun</p>
            </Card>
          )}
        </div>
      )}
      {activeLink === "vendo" && (
        <div>
          {posts.length > 0 ? (
            posts.map((post) => <Post key={post.created_at} {...post} />)
          ) : (
            <Card>
              <p className="mb-2 text-sm">No hay publicaciones aun</p>
            </Card>
          )}
        </div>
      )}
      {activeLink === "contacto" && <Contact />}
    </div>
  );
}

export default Section;
