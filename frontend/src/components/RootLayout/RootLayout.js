import Sidebar from "../Sidebar/Sidebar";

function RootLayout({ children, noSidebar }) {
  return (
    <main>
      <div className="flex justify-center">
        {!noSidebar && (
          <div className="absolute left-0">
            <Sidebar />
          </div>
        )}
        <div className={noSidebar ? "w-full" : "md:w-3/5 md:mx-0 w-full mx-2"}>
          {children}
        </div>
      </div>
    </main>
  );
}

export default RootLayout;
