import { useState } from "react";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import { LineWobble } from "@uiball/loaders";
import { updateImage } from "@/utils/helpers";

function Avatar({ large, small, url, editable, onChange }) {
  const [isUploading, setIsUploading] = useState(false);
  const supabase = useSupabaseClient();
  const session = useSession();

  const updateAvatar = async (e) => {
    const file = e.target.files?.[0];

    if (file && file.type.startsWith("image/")) {
      setIsUploading(true);
      await updateImage(
        supabase,
        session.user.id,
        file,
        "avatars",
        "avatar_url"
      );
      setIsUploading(false);

      if (onChange) onChange();
    }
  };

  return (
    <div className="relative border rounded-full">
      <div
        className={`rounded-full overflow-hidden bg-slate-400 ${
          large ? "w-28 h-28 sm:w-32 sm:h-32" : small ? "w-8 h-8" : "w-12 h-12"
        }`}
      >
        <img
          className="w-full h-full object-cover"
          src={
            url ||
            "https://stonegatesl.com/wp-content/uploads/2021/01/avatar.jpg"
          }
          alt="user avatar"
        />
      </div>
      {isUploading && (
        <div className="absolute inset-0 bg-white bg-opacity-80 flex items-center justify-center">
          <LineWobble size={100} color="#231F20" />
        </div>
      )}
      {editable && (
        <div className="absolute right-0 bottom-0 mx-1 my-2">
          <label className="bg-white rounded-full cursor-pointer p-1 text-md border">
            <input
              className="hidden"
              type="file"
              onChange={updateAvatar}
              accept="image/*"
            />
            <span>✏️</span>
          </label>
        </div>
      )}
    </div>
  );
}

export default Avatar;
