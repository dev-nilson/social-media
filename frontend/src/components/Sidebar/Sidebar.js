import { useContext } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { UserContext } from "@/contexts/UserContext";

function Sidebar() {
  const { pathname } = useRouter();
  const { profile } = useContext(UserContext);

  return (
    <nav className="fixed bottom-0 md:top-0 pt-4 w-screen md:w-20 xl:w-60 md:h-screen border-r p-3 bg-white z-50">
      <div className="text-md flex space-y-4 justify-between md:block">
        <Link className={`link ${pathname === "/" && "link--active"}`} href="/">
          <span className="text-2xl">🚩</span>
          <span className="hidden xl:block">Inicio</span>
        </Link>
        <Link
          className={`link ${pathname === "/promociones" && "link--active"}`}
          href="/promociones"
        >
          <span className="text-2xl">🔥</span>
          <span className="hidden xl:block">Promociones</span>
        </Link>
        <Link
          className={`link ${pathname === "/destacados" && "link--active"}`}
          href="/destacados"
        >
          <span className="text-2xl">⭐</span>
          <span className="hidden xl:block">Destacados</span>
        </Link>
        <Link
          className={`link ${pathname === "/loteria" && "link--active"}`}
          href="/loteria"
        >
          <span className="text-2xl">☘️</span>
          <span className="hidden xl:block">Lotería</span>
        </Link>
        <Link
          className={`link ${pathname === "/perfil" && "link--active"}`}
          href={`/${profile?.username}`}
        >
          <span className="text-2xl">👤</span>
          <span className="hidden xl:block">Perfil</span>
        </Link>
        <Link className="link" href="/">
          <span className="text-2xl">⬅️</span>
          <span className="hidden xl:block">Salir</span>
        </Link>
      </div>
    </nav>
  );
}

export default Sidebar;
