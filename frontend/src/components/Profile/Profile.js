import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import Card from "@/components/Card/Card";
import Cover from "@/components/Cover/Cover";
import Avatar from "@/components/Avatar/Avatar";
import Bio from "@/components/Bio/Bio";
import Links from "@/components/Links/Links";
import { getProfile } from "@/utils/helpers";

function Profile() {
  const [editMode, setEditMode] = useState(false);
  const [profile, setProfile] = useState(null);
  const [notFound, setNotFound] = useState(false);
  const router = useRouter();
  const session = useSession();
  const supabase = useSupabaseClient();

  const username = router.query.id;
  const section = router.query.section || [""];
  const isMyProfile = username === session?.user?.user_metadata?.username;

  useEffect(() => {
    if (!username) return;
    fetchProfile();
  }, [username]);

  const fetchProfile = () => {
    getProfile(supabase, username).then((res) => {
      if (res.data.length > 0) setProfile(res.data[0]);
      else setNotFound(true);
    });
  };

  return (
    <Card noPadding>
      <div className="relative overflow-hidden rounded-md">
        <Cover
          url={profile?.cover_url}
          editable={editMode}
          onChange={fetchProfile}
        />
        <div className="absolute top-12 sm:top-24 left-5">
          <Avatar
            url={profile?.avatar_url}
            editable={editMode}
            onChange={fetchProfile}
            large
          />
        </div>
        <div className="p-2 pb-2">
          {profile && (
            <Bio
              username={profile.username}
              description={profile.bio}
              editable={isMyProfile}
              editMode={editMode}
              setEditMode={setEditMode}
              onChange={fetchProfile}
            />
          )}
          <Links activeLink={section[0]} username={username} />
        </div>
      </div>
    </Card>
  );
}

export default Profile;
