import { useContext, useState } from "react";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import { LineWobble } from "@uiball/loaders";
import { UserContext } from "@/contexts/UserContext";
import Avatar from "../Avatar/Avatar";
import Card from "../Card/Card";
import Select from "../Select/Select";
import { createPost } from "@/utils/helpers";

const categories = [
  { label: "💵 Comprar", value: "buy" },
  { label: "🏷️ Vender", value: "sell" },
];

function PostForm({ onPost }) {
  const [content, setContent] = useState("");
  const [category, setCategory] = useState("");
  const [files, setFiles] = useState([]);
  const [isUploading, setIsUploading] = useState(false);
  const supabase = useSupabaseClient();
  const session = useSession();
  const { profile } = useContext(UserContext);

  const addPhotos = async (e) => {
    const files = e.target.files;

    if (files.length > 0) {
      setIsUploading(true);
      for (const file of files) {
        const fileName = Date.now() + file.name;

        const result = await supabase.storage
          .from("photos")
          .upload(fileName, file);

        if (result.data) {
          const url = `https://zlbaptomjwivldsofuqi.supabase.co/storage/v1/object/public/photos/${result.data.path}`;
          setFiles((prevFiles) => [...prevFiles, url]);
        }

        setIsUploading(false);
      }
    }
  };

  const postPost = async () => {
    const post = {
      username: session.user.user_metadata.username,
      author: session.user.id,
      content,
      photos: files,
      category,
    };

    await createPost(supabase, post);
    setContent("");
    setFiles([]);
    onPost();
  };

  return (
    <Card>
      <div className="flex gap-2 items-center border-b pb-3">
        <div>
          <Avatar url={profile?.avatar_url} />
        </div>
        <input
          className="grow p-3 h-fit"
          placeholder="Detalles de tu publicacion (talla, precio, etc)."
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />
        <Select
          options={categories}
          selectedOption={category}
          setSelectedOption={setCategory}
        />
      </div>
      <div className="flex flex-col gap-2">
        <div className="mt-2">
          {isUploading && <LineWobble size={100} color="#231F20" />}
          {files.length > 0 && (
            <div className="flex flex-wrap gap-2">
              {files.map((file) => (
                <img className="w-auto h-16 rounded-md" src={file} alt="" />
              ))}
            </div>
          )}
        </div>
        <div className="flex items-center w-full">
          <div>
            <label className="flex gap-1 items-center link cursor-pointer">
              <input
                type="file"
                className="hidden"
                onChange={addPhotos}
                multiple
              />
              🖼️
              <span className="hidden text-sm md:block">Imagenes</span>
            </label>
          </div>

          <div className="grow text-right">
            <label
              className="text-white bg-black px-4 py-1 rounded-md cursor-pointer"
              onClick={postPost}
            >
              Publicar
            </label>
          </div>
        </div>
      </div>
    </Card>
  );
}

export default PostForm;
