import React from "react";
import Masonry from "react-masonry-css";
import Card from "../Card/Card";

const images = [
  {
    id: 0,
    alt: "",
    url: "https://images.solecollector.com/complex/images/c_crop,h_1084,w_1927,x_46,y_539/c_fill,dpr_2.0,f_auto,fl_lossy,q_auto,w_680/p9s3n69befhkcjyerczx/air-jordan-1-high-og-womens-chenille-red-dj4891-061-pair",
  },
  {
    id: 1,
    alt: "",
    url: "https://media.vogue.mx/photos/63daae4eb338546ffface689/2:3/w_948,h_1422,c_limit/Capture%20d%E2%80%99e%CC%81cran%202023-01-31%20a%CC%80%2017.13.48.png",
  },
  {
    id: 2,
    alt: "",
    url: "https://cdn.shopify.com/s/files/1/0496/4325/8009/articles/nike-dunk-low-les-5-meilleurs-coloris-de-la-celebre-basket-disponibles-sur-kikikickz-179496.jpg?v=1674752374",
  },
  {
    id: 3,
    alt: "",
    url: "https://content.asos-media.com/-/media/images/articles/men/2017/05/14-sun/sneaker-sunday-dunk-high/asos-mw-dd-article-sneaker-sunday.jpg?h=398&w=398&la=en-GB&hash=54D051061B7E5672CE3F19E4BFA24AA3",
  },
  {
    id: 4,
    alt: "",
    url: "https://media.gq-magazine.co.uk/photos/633eab4246e9ddeb400c0d5e/3:2/w_1620,h_1080,c_limit/Nike-air-max.jpg",
  },
  {
    id: 5,
    alt: "",
    url: "https://media.revistagq.com/photos/6152da192efa7ebc613dcdca/4:3/w_1490,h_1117,c_limit/nike%20blazer%20precio%20historia.png",
  },
  {
    id: 6,
    alt: "",
    url: "https://cdn.shopify.com/s/files/1/0623/6684/3125/products/PHOTO-2022-06-10-21-44-32_1800x1800.jpg?v=1655347642",
  },
];

function Photos() {
  return (
    <Card>
      <Masonry
        breakpointCols={{
          default: 3,
          1100: 2,
          300: 1,
        }}
        className="masonry-grid flex"
        columnClassName="masonry-grid-column"
      >
        {images.map((image) => (
          <img
            key={image.id}
            src={image.url}
            alt={image.alt}
            className="w-full h-auto object-cover p-1 !rounded-lg"
          />
        ))}
      </Masonry>
    </Card>
  );
}

export default Photos;
