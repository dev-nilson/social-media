import { useEffect, useState } from "react";
import Link from "next/link";
import ReactTimeAgo from "react-time-ago";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import {
  dislikePost,
  getComments,
  getCommentsCount,
  getLikes,
  likePost,
} from "@/utils/helpers";
import Avatar from "../Avatar/Avatar";
import Card from "../Card/Card";
import Comment from "../Comment/Comment";

function Post({ content, created_at, photos, profiles, id }) {
  const [likes, setLikes] = useState([]);
  const [comments, setComments] = useState([]);
  const [commentsCount, setCommentsCount] = useState(false);
  const [commentMode, setCommentMode] = useState(false);
  const supabase = useSupabaseClient();
  const session = useSession();

  const alreadyLiked = !!likes.find((like) => like.user_id === session.user.id);

  useEffect(() => {
    getLikes(supabase, id).then((res) => {
      setLikes(res.data);
    });

    getCommentsCount(supabase, id).then((res) => {
      setCommentsCount(res.count);
    });
  }, []);

  const handleLike = async () => {
    if (alreadyLiked) await dislikePost(supabase, id, session.user.id);
    else await likePost(supabase, id, session.user.id);

    // get likes after either action
    getLikes(supabase, id).then((res) => {
      setLikes(res.data);
    });
  };

  const showComments = async () => {
    getComments(supabase, id).then((res) => {
      setComments(res.data);
    });
  };

  return (
    <Card>
      <div className="flex gap-3">
        <div>
          <Link href={`/${profiles.username}`}>
            <Avatar url={profiles.avatar_url} />
          </Link>
        </div>
        <div className="grow">
          <Link
            href={`/${profiles.username}`}
            className="font-semibold hover:underline"
          >
            <span>{profiles.username}</span>
          </Link>
          <p className="text-gray-500 text-xs">
            <ReactTimeAgo date={Date.parse(created_at)} />
          </p>
        </div>
      </div>
      <div>
        <p className="my-3 text-md">{content}</p>
        {photos.length > 0 &&
          photos.map((photo) => (
            <div
              key={photo}
              className="w-full h-[250px] md:h-[500px] m-auto rounded-md"
            >
              <img
                className="rounded-md overflow-hidden object-cover w-full h-full"
                src={photo}
              />
            </div>
          ))}
      </div>
      <div className="flex text-sm justify-between mt-2 pb-2 border-b">
        <span>❤️ {likes.length}</span>
        <span
          className="cursor-pointer hover:bg-gray-100 px-2 rounded-md"
          onClick={showComments}
        >
          {commentsCount} Comentarios
        </span>
      </div>
      <div className="flex gap-1 mt-1">
        <button className="flex gap-2 items-center link" onClick={handleLike}>
          <span className="text-2xl">{alreadyLiked ? "❤️" : "🤍"}</span>
          <span
            className={`hidden md:block text-sm font-semibold ${
              alreadyLiked && "text-red-600"
            }`}
          >
            Me Gusta
          </span>
        </button>
        <button
          className="flex gap-2 items-center link"
          onClick={() => setCommentMode((prev) => !prev)}
        >
          <span className="text-2xl">💬</span>
          <span className="hidden md:block text-sm font-semibold">
            Comentar
          </span>
        </button>
      </div>
      {comments.map((comment) => (
        <Comment avatar={profiles.avatar_url} id={id} text={comment.content} date={comment.created_at} />
      ))}
      {commentMode && <Comment avatar={profiles.avatar_url} postId={id} />}
    </Card>
  );
}

export default Post;
