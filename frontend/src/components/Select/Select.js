function Select({ options, selectedOption, setSelectedOption }) {
  return (
    <div className="flex space-x-2">
      {options.map((option) => (
        <label
          key={option.value}
          className={`flex text-sm items-center px-2 py-1 rounded-md border cursor-pointer ${
            selectedOption === option.value
              ? "bg-black text-white"
              : "border-gray-300"
          }`}
        >
          <input
            type="radio"
            value={option.value}
            checked={selectedOption === option.value}
            onChange={() => setSelectedOption(option.value)}
            className="sr-only"
          />
          {option.label}
        </label>
      ))}
    </div>
  );
}

export default Select;
