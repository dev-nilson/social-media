function Card({ children, noPadding }) {
  return (
    <div
      className={`bg-white rounded-lg border border-[#ECECEC] shadow-lg shadow-[#fafafa] mb-4 ${
        !noPadding && "p-4"
      }`}
    >
      {children}
    </div>
  );
}

export default Card;
