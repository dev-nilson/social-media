import { useState } from "react";
import { useSession, useSupabaseClient } from "@supabase/auth-helpers-react";
import { updateDescription } from "@/utils/helpers";

function Bio({ username, description, editable, editMode, setEditMode, onChange }) {
  const [usernameEdit, setUsernameEdit] = useState(username);
  const [descriptionEdit, setDescriptionEdit] = useState(description);
  const supabase = useSupabaseClient();
  const session = useSession();

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleSave = () => {
    updateDescription(supabase, descriptionEdit, session.user.id).then(() => {
      setEditMode(false);
      onChange();
    });
  };

  const handleCancel = () => {
    setEditMode(false);
  };

  return (
    <div className="ml-32 sm:ml-40 flex justify-between">
      <div className="flex flex-col">
        {editMode ? (
          <input
            className="font-bold border px-2 py-1 focus:outline-none rounded-md mb-1"
            type="text"
            placeholder="Descripcion"
            value={usernameEdit}
            onChange={(e) => setUsernameEdit(e.target.value)}
          />
        ) : (
          <h1 className="text-lg sm:text-2xl font-bold">{username}</h1>
        )}
        {editMode ? (
          <input
            className="border text-sm px-2 py-1 focus:outline-none rounded-md"
            type="text"
            placeholder="Descripcion"
            value={descriptionEdit}
            onChange={(e) => setDescriptionEdit(e.target.value)}
          />
        ) : (
          <p className="text-gray-600 text-sm">{description}</p>
        )}
      </div>
      {editable && !editMode && (
        <div>
          <button
            className="border px-2 py-1 rounded-md text-sm"
            onClick={handleEdit}
          >
            ✏️ <span className="hidden md:inline-flex">Editar Perfil</span>
          </button>
        </div>
      )}
      {editable && editMode && (
        <div className="flex gap-1 h-fit">
          <button
            className="border px-2 py-1 rounded-md text-sm"
            onClick={handleCancel}
          >
            ⛔ <span className="hidden md:inline-flex">Cancelar</span>
          </button>
          <button
            className="border px-2 py-1 rounded-md text-sm"
            onClick={handleSave}
          >
            💾 <span className="hidden md:inline-flex">Guardar</span>
          </button>
        </div>
      )}
    </div>
  );
}

export default Bio;
