import Link from "next/link";

function Links({ activeLink, username }) {
  return (
    <div className="mt-8 flex gap-1">
      <Link
        className={`link ${activeLink === "compro" && "link--active"}`}
        href={`/${username}/compro`}
      >
        <span className="text-2xl">💵</span>
        <span className="hidden sm:block text-sm font-semibold">
          Quiero Comprar
        </span>
      </Link>
      <Link
        className={`link ${activeLink === "vendo" && "link--active"}`}
        href={`/${username}/vendo`}
      >
        <span className="text-2xl">🏷️</span>
        <span className="hidden sm:block text-sm font-semibold">
          Quiero Vender
        </span>
      </Link>
      <Link
        className={`link ${activeLink === "contacto" && "link--active"}`}
        href={`/${username}/contacto`}
      >
        <span className="text-2xl">🌐</span>
        <span className="hidden sm:block text-sm font-semibold">Contacto</span>
      </Link>
    </div>
  );
}

export default Links;
