import Card from "../Card/Card";

function Contact() {
  return (
    <Card>
      <h2 className="mb-2 text-2xl">Contacto</h2>
      <p className="mb-2 text-sm">Redes sociales</p>
    </Card>
  );
}

export default Contact;
