export const updateImage = async (
  supabase,
  userId,
  file,
  bucket,
  imageType
) => {
  return new Promise(async (resolve, reject) => {
    const fileName = Date.now() + file.name;
    const result = await supabase.storage.from(bucket).upload(fileName, file);

    if (result.data) {
      const url = `https://zlbaptomjwivldsofuqi.supabase.co/storage/v1/object/public/${bucket}/${result.data.path}`;
      supabase
        .from("profiles")
        .update({ [imageType]: url })
        .eq("id", userId)
        .then((res) => {
          if (!res.error) resolve();
          else throw res.error;
        });
    }
  });
};

export const getLikes = async (supabase, postId) => {
  return await supabase.from("likes").select().eq("post_id", postId);
};

export const getComments = async (supabase, postId) => {
  return await supabase.from("comments").select().eq("post_id", postId);
};

export const getPostsByCategory = async (supabase, username, category) => {
  return await supabase
    .from("posts")
    .select(
      "id, content, created_at, photos, author, username, profiles(id, avatar_url, full_name, username)"
    )
    .order("created_at", { ascending: false })
    .eq("username", username)
    .eq("category", category);
};

export const getCommentsCount = async (supabase, postId) => {
  return await supabase
    .from("comments")
    .select("*", { count: "exact", head: true })
    .eq("post_id", postId);
};

export const dislikePost = async (supabase, postId, userId) => {
  await supabase
    .from("likes")
    .delete()
    .eq("post_id", postId)
    .eq("user_id", userId);
};

export const likePost = async (supabase, postId, userId) => {
  await supabase.from("likes").insert({
    post_id: postId,
    user_id: userId,
  });
};

export const commentPost = async (supabase, postId, userId, content) => {
  await supabase.from("comments").insert({
    post_id: postId,
    user_id: userId,
    content,
  });
};

export const getPosts = async (supabase) => {
  return await supabase
    .from("posts")
    .select(
      "id, content, created_at, photos, profiles(id, avatar_url, full_name, username)"
    )
    .order("created_at", { ascending: false });
};

export const createPost = async (supabase, post) => {
  await supabase.from("posts").insert(post);
};

export const getProfile = async (supabase, username) => {
  return await supabase.from("profiles").select().eq("username", username);
};

export const updateDescription = async (supabase, bio, userId) => {
  return await supabase.from("profiles").update({ bio }).eq("id", userId);
};
