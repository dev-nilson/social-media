import { useRouter } from "next/router";
import ProfileLayout from "@/components/ProfileLayout/ProfileLayout";
import Section from "@/components/Section/Section";

function ProfileSection() {
  const router = useRouter();
  const section = router.query.section || [""];
  const username = router.query.id;

  return (
    <ProfileLayout>
      <Section activeLink={section[0]} username={username} />
    </ProfileLayout>
  );
}

export default ProfileSection;
