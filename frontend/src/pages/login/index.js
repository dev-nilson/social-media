import { useSupabaseClient } from "@supabase/auth-helpers-react";
import { useState } from "react";

function index() {
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const supabase = useSupabaseClient();

  async function signInWithEmail() {
    try {
      await supabase.auth.signInWithOtp({
        email,
        options: {
          emailRedirectTo: "https://example.com/welcome",
          shouldCreateUser: true,
          data: {
            full_name: name,
            avatar_url:
              "https://alumni.engineering.utoronto.ca/files/2022/05/Avatar-Placeholder-400x400-1.jpg",
            username,
          },
        },
      });
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div>
      <input
        value={name}
        placeholder="name"
        onChange={(e) => setName(e.target.value)}
      />
      <input
        value={username}
        placeholder="username"
        onChange={(e) => setUsername(e.target.value)}
      />
      <input
        value={email}
        placeholder="email"
        onChange={(e) => setEmail(e.target.value)}
      />
      <button onClick={signInWithEmail}>Submit</button>
    </div>
  );
}

export default index;
