import { useEffect, useState } from "react";
import { Inter } from "next/font/google";
import { useSupabaseClient } from "@supabase/auth-helpers-react";
import Head from "next/head";
import PostForm from "@/components/PostForm/PostForm";
import Post from "@/components/Post/Post";
import RootLayout from "@/components/RootLayout/RootLayout";
import { getPosts } from "@/utils/helpers";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const supabase = useSupabaseClient();
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = () => {
    getPosts(supabase).then((res) => {
      setPosts(res.data);
    });
  };

  return (
    <>
      <Head>
        <title>Social Media</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <RootLayout>
        <div className="max-w-2xl m-auto">
          <PostForm onPost={fetchPosts} />
          {posts.map((post) => (
            <Post key={post.created_at} {...post} />
          ))}
        </div>
      </RootLayout>
    </>
  );
}
