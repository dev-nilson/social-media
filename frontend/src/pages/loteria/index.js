import RootLayout from "@/components/RootLayout/RootLayout";

function Saved() {
  return (
    <RootLayout>
      <h1 className="text-2xl mb-4 text-gray-400">Lotería</h1>
    </RootLayout>
  );
}

export default Saved;
