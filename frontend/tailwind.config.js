/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        socialBg: "#f5f7fb",
        socialBlue: "#218dfa"
      }
    },
  },
  plugins: [],
}
